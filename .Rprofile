# use RStudio's Public Package Manager to download binary packages for Ubuntu 18.04 (Bionic)
options(repos = c(CRAN = "https://packagemanager.rstudio.com/all/__linux__/bionic/latest"))

# install packages in parallel;
# value identified via `parallelly::availableCores()` instead of `parallel::detectCores()`;
# see https://www.jumpingrivers.com/blog/speeding-up-package-installation/
# and https://www.jottr.org/2022/12/05/avoid-detectcores/
options(Ncpus = 1)

# source: https://kevinushey.github.io/blog/2015/02/02/rprofile-essentials/
options(
  warnPartialMatchArgs = TRUE, # warn on partial matches
  warnPartialMatchDollar = TRUE,
  warnPartialMatchAttr = TRUE,
  warn = 2, # warnings are errors
  lifecycle_verbosity = "error", # for packages using {lifecycle}
  useFancyQuotes = FALSE, # fancy quotes lead to copy+paste bugs
  papersize = "a4",
  max.print = 100,
  datatable.print.class = TRUE, # improve {data.table} print
  datatable.print.keys = TRUE
)

if(interactive()) {
  # print tables colorful
  print.data.frame <- colorDF:::print_colorDF
  print.tbl <- colorDF:::print_colorDF
  print.data.table <- colorDF:::print_colorDF
  options(colorDF_tibble_style = TRUE)

  # print functions colorful
  prettycode::prettycode()

  # print everything else colorful
  colorout::setOutputColors(normal = 0, number = 2, string = 1, const = 5)

  if (Sys.getenv("RSTUDIO") != "1" || Sys.getenv("RSTUDIO_TERM") != "") {
    # print help pages colorful
    rdoc::use_rdoc()
  }
}
